import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import { SideBar } from '../../../src/components/SideBar/SideBar';

it('renders correctly', () => {
  const component = renderer.create(<SideBar />);
  expect(component.toJSON()).toMatchSnapshot();
});
