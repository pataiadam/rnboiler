import 'react-native';
import React from 'react';
import {create} from 'react-test-renderer';
import * as ReactRedux from 'react-redux';

import {Main} from '../../src/views/Main';

describe('Button component', () => {
  it('Matches the snapshot', () => {
    jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue('mock');
    const component = create(<Main navigation={{navigate: () => {}}} />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
