import 'react-native';
import React from 'react';
import {create} from 'react-test-renderer';
import * as ReactRedux from 'react-redux';

import {Auth} from '../../src/views/Auth';

describe('Button component', () => {
  it('Matches the snapshot', () => {
    jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue('mock');
    jest.spyOn(ReactRedux, 'useSelector').mockReturnValue('mock');
    const component = create(<Auth navigation={{navigate: () => {}}} />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
