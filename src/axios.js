import axios from 'axios';
import config from './config';
import {store} from './redux/store';

let id = 10000;
axios.defaults.baseURL = config.HOST_URL;
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.responseType = 'json';
axios.interceptors.request.handlers = [];
axios.interceptors.response.handlers = [];

axios.interceptors.request.use(request => {
  const isAuthed = store.getState().authReducer.isAuthed;
  const user = store.getState().authReducer.user;
  const authedRequest = isAuthed && user && user.token;

  if (authedRequest) {
    request.headers.authorization = `Bearer ${user.token}`;
  }

  request.id = ++id;
  console.log(
    `${authedRequest ? '[AUTHED]' : '[NOT-AUTHED]'} Request (#${request.id})`,
    request,
  );
  return request;
});

axios.interceptors.response.use(response => {
  console.log(`Response (#${response.config.id})`, response.data);
  return response;
});

global.axios = axios;
