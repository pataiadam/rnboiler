import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import AuthLoading from './AuthLoadingScreen';
import {authNavConfig, appNavConfig, appStackConfig} from './_navigatorConfigs';

import Auth from '../views/Auth';
import Main from '../views/Main';

const AuthNav = createStackNavigator(
  {
    Main: {screen: Auth},
  },
  authNavConfig,
);

const AppStack = createStackNavigator(
  {
    Main: {screen: Main},
  },
  appStackConfig,
);

const AppNav = createDrawerNavigator(
  {
    AppStack,
  },
  appNavConfig,
);

const SwitchNav = createSwitchNavigator(
  {
    AuthLoading,
    AppNav,
    AuthNav,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

export default createAppContainer(SwitchNav);
