import React from 'react';
import {ActivityIndicator, View} from 'react-native';

import {store} from '../redux/store';

class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const {isAuthed} = store.getState().authReducer;
    this.props.navigation.navigate(isAuthed ? 'AppNav' : 'AuthNav');
  };

  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}

export default AuthLoadingScreen;
