import React from 'react';

import SideBar from '../components/SideBar/SideBar';

function forVertical(props) {
  const {layout, position, scene} = props;

  const index = scene.index;
  const width = layout.initWidth;

  const translateX = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0, 0, -width],
  });
  const translateY = 0;

  const opacity = position.interpolate({
    inputRange: [index - 1, index - 0.99, index],
    outputRange: [0, 0, 1],
  });
  return {
    opacity,
    transform: [{translateX}, {translateY}],
  };
}

export const authNavConfig = {
  initialRouteName: 'Main',
  headerMode: 'none',
  transparentCard: true,
  transitionConfig: () => ({
    containerStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
    },
  }),
};

export const appStackConfig = {
  headerMode: 'none',
  transparentCard: true,
  transitionConfig: () => ({
    screenInterpolator: forVertical,
    containerStyle: {
      backgroundColor: 'transparent',
    },
  }),
};

export const appNavConfig = {
  contentComponent: SideBar,
};
