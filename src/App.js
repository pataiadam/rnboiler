import React from 'react';
import {StatusBar} from 'react-native';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';

import I18n from './locales';
import './axios';
import Layout from './routes';
import {store, persistor} from './redux/store';

const App = () => {
  return (
    <>
      <StatusBar barStyle="light-content" hidden={true} translucent={true} />
      <Provider store={store}>
        <PersistGate
          onBeforeLift={async () => {
            await I18n.changeLanguage(store.getState().appReducer.lang);
          }}
          loading={null}
          persistor={persistor}>
          <Layout />
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;
