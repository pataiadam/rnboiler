import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

import packageJson from '../../../package.json';

export const SideBar = props => {
  return (
    <View style={styles.container}>
      <View style={styles.content} />
      <Text style={styles.versionText}>{packageJson.version}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#252335',
  },
  content: {
    flex: 1,
  },
  versionText: {
    color: '#fff',
    alignSelf: 'flex-end',
    marginRight: 5,
    marginBottom: 5,
  },
});

export default SideBar;
