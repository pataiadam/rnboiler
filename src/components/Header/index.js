import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {NavigationActions} from 'react-navigation';

const BACK_ICON = require('../../assets/images/icon_back.png');
const MENU_ICON = require('../../assets/images/icon_menu.png');

const Header = props => {
  const back = () => {
    props.navigation.dispatch(NavigationActions.back());
  };

  const open = () => {
    props.navigation.openDrawer();
  };

  const title = props.title || '';
  const isBack = props.isBack || false;
  return (
    <View style={styles.container}>
      {isBack ? (
        <TouchableOpacity style={{padding: 10}} onPress={back}>
          <Image source={BACK_ICON} style={styles.icon} />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity style={{padding: 10}} onPress={open}>
          <Image source={MENU_ICON} style={styles.icon} />
        </TouchableOpacity>
      )}
      <View>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={{width: 32}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#22242A',
  },
  title: {
    color: '#FC600A',
    fontSize: 24,
    fontWeight: '700',
  },
  icon: {
    height: 32,
    aspectRatio: 1,
  },
});

export default Header;
