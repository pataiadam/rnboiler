import i18next from 'i18next';

import en from './en';
import hu from './hu';

i18next.init({
  resources: {
    en: {translation: en},
    hu: {translation: hu},
  },
});

global.__ = i18next.t.bind(i18next);

export default i18next;
