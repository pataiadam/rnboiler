import {types} from './action';

const initialState = {
  lang: 'en',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SELECT_LANGUAGE:
      return {
        ...state,
        lang: action.lang,
      };

    default:
      return state;
  }
};

export const loadingReducer = (state = {}, action) => {
  const {type} = action;
  const matches = /(.*)_(REQUEST|SUCCESS|FAILURE)/.exec(type);

  if (!matches) return state;

  const [, requestName, requestState] = matches;
  return {
    ...state,
    [requestName]: requestState === 'REQUEST',
  };
};

export const errorReducer = (state = {}, action) => {
  const {type, payload} = action;
  const matches = /(.*)_(REQUEST|FAILURE)/.exec(type);

  if (!matches) return state;

  const [, requestName, requestState] = matches;
  // stores errorMessage when receiving GET_TODOS_FAILURE
  // else clear errorMessage when receiving GET_TODOS_REQUEST
  return {
    ...state,
    [requestName]: requestState === 'FAILURE' ? payload.message : '',
  };
};
