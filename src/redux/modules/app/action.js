import I18n from '../../../locales';

export const types = {
  SELECT_LANGUAGE: 'SELECT_LANGUAGE',
};

export const selectLanguage = lang => async dispatch => {
  await I18n.changeLanguage(lang);
  dispatch({
    type: types.SELECT_LANGUAGE,
    lang,
  });
};

export const createErrorMessageSelector = actions => state => {
  const [error] = actions.map(action => state[action]);
  return error || '';
};

export const createLoadingSelector = actions => state =>
  actions.some(action => state[action]);
