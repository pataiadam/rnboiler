import {combineReducers} from 'redux';

import appReducer, {loadingReducer, errorReducer} from './app/reducer';
import authReducer from './auth/reducer';

export default combineReducers({
  appReducer,
  loadingReducer,
  errorReducer,
  authReducer,
});
