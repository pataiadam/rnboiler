import {types} from './action';

// IMPORTANT: isAuthed and user.token (Bearer {token}) must be maintained (see src/axios.js)
const initialState = {
  isAuthed: false,
  user: {token: null},
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthed: true,
      };
    case types.LOGOUT:
      return {
        ...state,
        isAuthed: false,
        user: initialState.user,
      };

    default:
      return state;
  }
};
