import AuthService from './_service';

export const types = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAILURE: 'LOGIN_FAILURE',

  LOGOUT: 'LOGOUT',
};

export const login = fail => async dispatch => {
  dispatch({type: types.LOGIN_REQUEST});
  const response = await AuthService.login();
  if (!fail) {
    // FIXME determine success responses eg. if (response.isSuccess)
    return dispatch({type: types.LOGIN_SUCCESS, response});
  }
  return dispatch({
    type: types.LOGIN_FAILURE,
    payload: {message: 'Error message'},
  });
};

export const logout = () => async dispatch => {
  dispatch({type: types.LOGOUT});
};
