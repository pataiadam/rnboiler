import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import thunk from 'redux-thunk';

import rootReducer from '../modules';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  // these reducers will be persisted
  whitelist: ['appReducer', 'authReducer'],
  // these reducers won't be persisted
  blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(thunk));
let persistor = persistStore(store);
export {store, persistor};
