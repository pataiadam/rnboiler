import {useDispatch} from 'react-redux';
import React from 'react';
import {Button, SafeAreaView, StyleSheet, ScrollView} from 'react-native';

import Header from '../components/Header';
import {logout} from '../redux/modules/auth/action';

export const Main = props => {
  const dispatch = useDispatch();

  const signOut = async () => {
    dispatch(logout());
    props.navigation.navigate('AuthNav');
  };

  return (
    <SafeAreaView style={styles.container}>
      <Header navigation={props.navigation} title={'Header'} />
      <ScrollView style={styles.content}>
        <Button title="Sign out!" onPress={signOut} />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    padding: 10,
  },
});

export default Main;
