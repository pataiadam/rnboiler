import {useDispatch, useSelector} from 'react-redux';
import React, {useEffect} from 'react';
import {SafeAreaView, Button, Text, StyleSheet} from 'react-native';

import {
  selectLanguage,
  createLoadingSelector,
  createErrorMessageSelector,
} from '../redux/modules/app/action';
import {login} from '../redux/modules/auth/action';

export const Auth = props => {
  const dispatch = useDispatch();
  const lang = useSelector(state => state.appReducer.lang);
  const isAuthed = useSelector(state => state.authReducer.isAuthed);
  const isFetching = useSelector(state =>
    createLoadingSelector(['LOGIN'])(state.loadingReducer),
  );
  const error = useSelector(state =>
    createErrorMessageSelector(['LOGIN'])(state.errorReducer),
  );

  const signIn = fail => dispatch(login(fail));
  const changeLang = () => dispatch(selectLanguage({en: 'hu', hu: 'en'}[lang]));

  useEffect(() => {
    if (isAuthed) {
      props.navigation.navigate('AppNav');
    }
  }, [isAuthed]);

  return (
    <SafeAreaView style={styles.container}>
      <Text>{isFetching ? 'FETCHING' : error ? error : 'Ok'}</Text>
      <Button title="Sign in!" onPress={() => signIn()} />
      <Button title="Sign in! (FAIL)" onPress={() => signIn(true)} />
      <Button title={__('Language')} onPress={changeLang} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'space-around',
  },
});

export default Auth;
