# RN Boilerplate

## Used modules

- **Redux**: `react-redux redux redux-persist redux-thunk`
- **Navigation**: `react-navigation react-navigation-drawer react-navigation-stack`
- **Locale**: `i18next`
- **Network**: `axios`
- **Test**: `jest`

## Install
If you have curl, git and npx just use the following command, and *skip Manual install*
```bash
curl -L https://gitlab.com/pataiadam/rnboiler/-/raw/master/install.sh | bash
```

## Manual install
#### Clone this repo
```
git clone https://gitlab.com/pataiadam/rnboiler.git <ProjectName>
cd <ProjectName>
rm -rf .git && git init
```

#### Rename the app:
```
npx react-native-rename "Project Name" -b com.awesome_project

# OR

yarn global add react-native-rename
react-native-rename "Project Name" -b com.awesome_project

# open xcode
# update `Bundle Identifier`
```

#### Install packages:
```
yarn 
#or npm i
cd ios && pod deintegrate && pod install && cd ..
cp .env.example .env
```

## Run
```
npx react-native run-android
npx react-native run-ios
```

## Android prod build

### A, Generating an upload key
If you already have an upload key, just copy it into the `android/app` and go to B.

```
cd android/app
sudo keytool -genkey -v -keystore my-upload-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
cd ../..
```

### B, Build

```
cd android
nano gradle.properties
```

Edit the following (replace ***** with the correct keystore password, alias and key password),

```
MYAPP_UPLOAD_STORE_FILE=my-upload-key.keystore
MYAPP_UPLOAD_KEY_ALIAS=my-key-alias
MYAPP_UPLOAD_STORE_PASSWORD=******
MYAPP_UPLOAD_KEY_PASSWORD=******
```

Run the following command:

```
./gradlew bundleRelease
```

### C, Testing release build

Remove installed version of the app from your phone. You can kill any running packager.

```
react-native run-android --variant=release
```

Note that --variant=release is only available if you've set up signing as described above.

## Clean All

```
cd ios && pod deintegrate && rm -rf build && pod install && cd ..
cd android && ./gradlew clean && cd ..
```
