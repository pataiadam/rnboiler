#!/usr/bin/env bash

echo Your app\'s name \(eg. My Awesome App\)?
read name </dev/tty
if [ -z "$name" ]; then
      echo "Name cant be empty, try again!"
      exit
fi
echo Your bundle identifier \(eg. com.organization.awesome_app\)?
read bundle </dev/tty
if [ -z "$bundle" ]; then
      echo "Identifier cant be empty, try again!"
      exit
fi
echo Your project folder\'s name \(in upper camel case - eg. MyAwesomeApp\)?
read folder </dev/tty
if [ -z "$folder" ]; then
      echo "Folder name cant be empty, try again!"
      exit
fi

while true; do
    echo Do you wish to clone the app here: `pwd`/$folder ? yes/no
    read yn </dev/tty
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo 'Install...'
git clone https://gitlab.com/pataiadam/rnboiler.git $folder
cd $folder
rm -f install.sh
rm -rf .git
git init
cp .env.example .env

npx react-native-rename "$name" -b $bundle

echo Do you wish to install dependencies now?
echo 1, yes - with npm?
echo 2, yes - with yarn?
echo 3, no
while true; do
    read dep </dev/tty
    case $dep in
        1 ) rm -f yarn.lock; npm i; break;;
        2 ) rm -f package-lock.json; yarn; break;;
        3 ) echo "  0, (required) Install dependencies: yarn / npm i"; break;;
        * ) echo "Please answer with number 1, 2, or 3";;
    esac
done

echo Installation finished 🎉 🎉 🎉
echo What to do next:
echo ""
echo "  cd "$folder
echo ""
echo "🚀 For Android:"
echo "    1, npx react-native run-android"
echo ""
echo "🚀 for iOS"
echo "    1, Install pod dependencies:"
echo "        cd ios && pod deintegrate && pod install && cd .."
echo ""
echo "    2, Open xcode and update Bundle Identifier"
echo ""
echo "    3, npx react-native run-ios"
echo "🖐"
